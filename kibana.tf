resource "helm_release" "kibana" {
  depends_on = [
    helm_release.elasticsearch
  ]
  name             = "kibana"
  namespace        = "logging"
  create_namespace = "true"
  repository       = "https://charts.bitnami.com/bitnami"
  chart            = "kibana"

  values = [
    templatefile("${path.module}/kibana-values-tmpl.yaml", {
      storage_class           = var.storage_class
      kibana_ingress_hostname = var.kibana_ingress_hostname
    })
  ]
}