resource "helm_release" "elasticsearch" {
  name             = "elasticsearch"
  namespace        = "logging"
  create_namespace = "true"
  repository       = "https://charts.bitnami.com/bitnami"
  chart            = "elasticsearch"

  set {
    name  = "global.storageClass"
    value = "${var.storage_class}"
  }
}