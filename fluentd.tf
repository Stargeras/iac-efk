resource "helm_release" "fluentd" {
  depends_on = [
    kubectl_manifest.fluentd-configmap
  ]
  name             = "fluentd"
  namespace        = "logging"
  create_namespace = "true"
  repository       = "https://charts.bitnami.com/bitnami"
  chart            = "fluentd"

  values = [
    templatefile("${path.module}/fluentd-values-tmpl.yaml", {
      storage_class    = var.storage_class
    })
  ]
}

resource "kubectl_manifest" "fluentd-configmap" {
  depends_on = [helm_release.elasticsearch]
  yaml_body  = <<YAML
apiVersion: v1
kind: ConfigMap
metadata:
  name: elasticsearch-output
  namespace: logging
data:
  fluentd.conf: |
    # Prometheus Exporter Plugin
    # input plugin that exports metrics
    <source>
      @type prometheus
      port 24231
    </source>

    # input plugin that collects metrics from MonitorAgent
    <source>
      @type prometheus_monitor
      <labels>
        host $${hostname}
      </labels>
    </source>

    # input plugin that collects metrics for output plugin
    <source>
      @type prometheus_output_monitor
      <labels>
        host $${hostname}
      </labels>
    </source>

    # Ignore fluentd own events
    <match fluent.**>
      @type null
    </match>

    # TCP input to receive logs from the forwarders
    <source>
      @type forward
      bind 0.0.0.0
      port 24224
    </source>

    # HTTP input for the liveness and readiness probes
    <source>
      @type http
      bind 0.0.0.0
      port 9880
    </source>

    # Throw the healthcheck to the standard output instead of forwarding it
    <match fluentd.healthcheck>
      @type stdout
    </match>

    # Send the logs to the standard output
    <match **>
      @type elasticsearch
      include_tag_key true
      host "#{ENV['ELASTICSEARCH_HOST']}"
      port "#{ENV['ELASTICSEARCH_PORT']}"
      logstash_format true
      verify_es_version_at_startup false
      default_elasticsearch_version 7

      <buffer>
        @type file
        path /opt/bitnami/fluentd/logs/buffers/logs.buffer
        flush_thread_count 2
        flush_interval 5s
      </buffer>
    </match>
YAML
}